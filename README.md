# README #

why is the online editor of bitbucket so frustrating?

### What is this repository for? ###

Connect ruuvitag sensors to a Raspberry Pi Zero W and push the data to Google Cloud IoT Core.

### How do I get set up? ###

Get the BLE MAC address of your Ruuvitag.
Add the MAC address in the code (there is an array somewhere called macs)
Create the following encryption files:
roots.pem
rsa_private.pem
rsa_cert.pem

Add the certs to Google IoT Core and with your sensor code on the raspberry pi

Make sure you have the required python packages:
pip install ruuvitag_sensor
pip install paho-mqtt
probably others as well, not sure which. Just check the errors

Run the code with the following command:
python sensor-gateway.py --project_id <<PROJECT_ID>> --registry_id <<IOT_REGISTRY>> --device_id <<DEVICE_ID>> --private_key_file rsa_private.pem --algorithm RS256

### Contribution guidelines ###
Kees van Bemmel

### Who do I talk to? ###
Mostly people who do stuff on computers