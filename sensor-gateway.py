#Copyright 2017 Google Inc. All rights reserved.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
r"""Sample device that consumes configuration from Google Cloud IoT.
This example represents a simple device with a temperature sensor and a fan
(simulated with software). When the device's fan is turned on, its temperature
decreases by one degree per second, and when the device's fan is turned off, its
temperature increases by one degree per second.
Every second, the device publishes its temperature reading to Google Cloud IoT
Core. The server meanwhile receives these temperature readings, and decides
whether to re-configure the device to turn its fan on or off. The server will
instruct the device to turn the fan on when the device's temperature exceeds 10
degrees, and to turn it off when the device's temperature is less than 0
degrees. In a real system, one could use the cloud to compute the optimal
thresholds for turning on and off the fan, but for illustrative purposes we use
a simple threshold model.
To connect the device you must have downloaded Google's CA root certificates,
and a copy of your private key file. See cloud.google.com/iot for instructions
on how to do this. Run this script with the corresponding algorithm flag.
  $ python cloudiot_pubsub_example_mqtt_device.py \
      --project_id=my-project-id \
      --registry_id=example-my-registry-id \
      --device_id=my-device-id \
      --private_key_file=rsa_private.pem \
      --algorithm=RS256
With a single server, you can run multiple instances of the device with
different device ids, and the server will distinguish them. Try creating a few
devices and running them all at the same time.
"""
import argparse
import json
import time
from urllib import quote
from datetime import datetime, timedelta
import copy
import requests
import jwt
import paho.mqtt.client as mqtt
from ruuvitag_sensor.ruuvi import RuuviTagSensor
from ruuvitag_sensor.ruuvitag import RuuviTag
from ruuvitag_sensor.decoder import UrlDecoder
all_data = {}
def create_jwt(project_id, private_key_file, algorithm):
  """Create a JWT (https://jwt.io) to establish an MQTT connection."""
  token = {
      'iat': datetime.utcnow(),
      'exp': datetime.utcnow() + timedelta(minutes=60),
      'aud': project_id
  }
  with open(private_key_file, 'r') as f:
    private_key = f.read()
  print 'Creating JWT using {} from private key file {}'.format(
      algorithm, private_key_file)
  return jwt.encode(token, private_key, algorithm=algorithm)
def error_str(rc):
  """Convert a Paho error to a human readable string."""
  return '{}: {}'.format(rc, mqtt.error_string(rc))
  
"
import argparse
import json
import time
from urllib import quote
from datetime import datetime, timedelta
import copy
import requests
import jwt
import paho.mqtt.client as mqtt
from ruuvitag_sensor.ruuvi import RuuviTagSensor
from ruuvitag_sensor.ruuvitag import RuuviTag
from ruuvitag_sensor.decoder import UrlDecoder
all_data = {}
def create_jwt(project_id, private_key_file, algorithm):
  """Create a JWT (https://jwt.io) to establish an MQTT connection."""
  token = {
      'iat': datetime.utcnow(),
      'exp': datetime.utcnow() + timedelta(minutes=60),
      'aud': project_id
  }
  with open(private_key_file, 'r') as f:
    private_key = f.read()
  print 'Creating JWT using {} from private key file {}'.format(
      algorithm, private_key_file)
  return jwt.encode(token, private_key, algorithm=algorithm)
def error_str(rc):
  """Convert a Paho error to a human readable string."""
  return '{}: {}'.format(rc, mqtt.error_string(rc))
class Device(object):
  """Represents the state of a single device."""
  def __init__(self):
    self.temperature = 0
    self.fan_on = False
    self.connected = False
  def update_sensor_data(self):
    """Pretend to read the device's sensor data.
    If the fan is on, assume the temperature decreased one degree,
    otherwise assume that it increased one degree.
    """
    if self.fan_on:
      self.temperature -= 1
    else:
      self.temperature += 1
  def wait_for_connection(self, timeout):
    """Wait for the device to become connected."""
    total_time = 0
    while not self.connected and total_time < timeout:
      time.sleep(1)
      total_time += 1
    if not self.connected:
      raise RuntimeError('Could not connect to MQTT bridge.')
  def on_connect(self, unused_client, unused_userdata, unused_flags, rc):
    """Callback for when a device connects."""
    print 'Connection Result:', error_str(rc)
    self.connected = True
  def on_disconnect(self, unused_client, unused_userdata, rc):
    """Callback for when a device disconnects."""
    print 'Disconnected:', error_str(rc)
    self.connected = False
  def on_publish(self, unused_client, unused_userdata, unused_mid):
    """Callback when the device receives a PUBACK from the MQTT bridge."""
    print 'Published message acked.'
     def on_subscribe(self, unused_client, unused_userdata, unused_mid,
                   granted_qos):
    """Callback when the device receives a SUBACK from the MQTT bridge."""
    print 'Subscribed: ', granted_qos
    if granted_qos[0] == 128:
      print 'Subscription failed.'
  def on_message(self, unused_client, unused_userdata, message):
    """Callback when the device receives a message on a subscription."""
    payload = str(message.payload)
    print "Received message '{}' on topic '{}' with Qos {}".format(
        payload, message.topic, str(message.qos))
    # The device will receive its latest config when it subscribes to the config
    # topic. If there is no configuration for the device, the device will
    # receive an config with an empty payload.
    if not payload:
      return
    # The config is passed in the payload of the message. In this example, the
    # server sends a serialized JSON string.
    data = json.loads(payload)
    if data['fan_on'] != self.fan_on:
      # If we're changing the state of the fan, print a message and update our
      # internal state.
      self.fan_on = data['fan_on']
      if self.fan_on:
        print 'Fan turned on.'
      else:
        print 'Fan turned off.'
        
def parse_command_line_args():
  """Parse command line arguments."""
  parser = argparse.ArgumentParser(
      description='Example Google Cloud IoT MQTT device connection code.')
  parser.add_argument(
      '--project_id', required=True, help='GCP cloud project name')
  parser.add_argument(
      '--registry_id', required=True, help='Cloud IoT registry id')
  parser.add_argument('--device_id', required=True, help='Cloud IoT device id')
  parser.add_argument(
      '--private_key_file', required=True, help='Path to private key file.')
  parser.add_argument(
      '--algorithm',
      choices=('RS256', 'ES256'),
      required=True,
      help='Which encryption algorithm to use to generate the JWT.')
  parser.add_argument(
      '--cloud_region', default='europe-west1', help='GCP cloud region')
  parser.add_argument(
      '--ca_certs',
      default='roots.pem',
      help='CA root certificate. Get from https://pki.google.com/roots.pem')
  parser.add_argument(
      '--num_messages',
      type=int,
      default=100,
      help='Number of messages to publish.')
  parser.add_argument(
      '--mqtt_bridge_hostname',
      default='mqtt.googleapis.com',
      help='MQTT bridge hostname.')
  parser.add_argument(
      '--mqtt_bridge_port', default=8883, help='MQTT bridge port.')
  return parser.parse_args()
def main():
  args = parse_command_line_args()
  runner(args)
  #localrunner()
def localrunner():
  full_data = '043E2A0201030157168974A51F0201060303AAFE1716AAFE10F9037275752E76692F23416A5558314D417730C3'
  data = full_data[26:]
  # convert_data returns tuple which has Data Format type and encoded data
  encoded = RuuviTagSensor.convert_data(data)
  sensor_data = UrlDecoder().decode_data(encoded[1])
  print(sensor_data)
  # run for 45 minutes then start over
  t_end = time.time() + 60 * 1
  while time.time() < t_end:
    # List of macs of sensors which data will be collected
    # If list is empty, data will be collected for all found sensors
    macs = ['F3:9E:F2:3F:E9:63', 'EE:47:8E:E6:D2:B2', 'D5:DB:AE:41:1C:A3', 'F9:36:24:D8:C8:0E', 'C3:3D:C3:13:17:D4']
    # get_data_for_sensors will look data for the duration of timeout_in_sec
    timeout_in_sec = 4
    datas = RuuviTagSensor.get_data_for_sensors(macs, timeout_in_sec)
    data_string = json.dumps(datas)
    print(data_string)
    time.sleep(10)
  print 'Stopping local runner'
  
def runner(args):
  # print 'projects/{}/locations/{}/registries/{}/devices/{}'.format(args.project_id, args.cloud_region, args.registry_id, args.device_id)
  # Create our MQTT client and connect to Cloud IoT.
  client = mqtt.Client(
      client_id='projects/{}/locations/{}/registries/{}/devices/{}'.format(
          args.project_id, args.cloud_region, args.registry_id, args.device_id))
  client.username_pw_set(
      username='unused',
      #password='eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1MDA2MjgyODUsImF1ZCI6ImluY2VudHJvLWNvcmUtaW90IiwiZXhwIjoxNTAwNjMxODg1fQ.KvPxPlx_g0NXlKD5Wm3WCgye2RC3Demh-NBvPmK1IaO3nDV5uOiGsvM8eudgawvPodyWMnQ7bhW7GF-L-AQg0f1JQqPUNpdN4OTnSNjzitBzwrxQsYa5KKhBrqZF4G$
      password=create_jwt(args.project_id, args.private_key_file,
                          args.algorithm))
  client.tls_set(ca_certs=args.ca_certs)
  device = Device()
  client.on_connect = device.on_connect
  client.on_publish = device.on_publish
  client.on_disconnect = device.on_disconnect
  client.on_subscribe = device.on_subscribe
  client.on_message = device.on_message
  client.connect(args.mqtt_bridge_hostname, args.mqtt_bridge_port)
  client.loop_start()
  # This is the topic that the device will publish telemetry events (temperature
  # data) to.
  mqtt_telemetry_topic = '/devices/{}/events'.format(args.device_id)
  # This is the topic that the device will receive configuration updates on.
  mqtt_config_topic = '/devices/{}/config'.format(args.device_id)
  # Wait up to 5 seconds for the device to connect.
  device.wait_for_connection(5)
  # Subscribe to the config topic.
  #client.subscribe(mqtt_config_topic, qos=1)
  # run for 45 minutes then start over
  t_end = time.time() + 60 * 45
  while time.time() < t_end:
    macs = ['F3:9E:F2:3F:E9:63', 'EE:47:8E:E6:D2:B2', 'D5:DB:AE:41:1C:A3', 'F9:36:24:D8:C8:0E', 'C3:3D:C3:13:17:D4', 'F4:83:2E:22:6B:11']
    # get_data_for_sensors will look data for the duration of timeout_in_sec
    datas = RuuviTagSensor.get_data_for_sensors(macs, 10)
    current_time = datetime.now()
    for key, value in datas.items():
        if key in all_data and all_data[key]['data'] == value:
            continue
        all_data[key] = {'mac': key, 'data': value, 'timestamp': current_time}
    new_data = {key: value for key, value in all_data.items() if value['timestamp'] is current_time}
    for mac, data in new_data.items():
        data_copy = copy.copy(data)
        data_copy['timestamp'] = current_time.isoformat()
        data_dump = json.dumps(data_copy)
        print(data_dump)
        client.publish(mqtt_telemetry_topic, data_dump, qos=1)
    not_found = [key for key, value in all_data.items() if value['timestamp'] < datetime.now() - timedelta(minutes=10)]
       for key in not_found:
        # TODO: Notify of lost sensors
        del all_data[key]
  client.disconnect()
  client.loop_stop()
  print 'Creating new JWT and starting over'
  runner(args)
if __name__ == '__main__':
  main()